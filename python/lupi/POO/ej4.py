class asignatura:
  def __init__(self, nombre, ano, especialidad, docente, unidades, bibliografia):
    self.nombre = nombre
    self.ano = ano
    self.especialidad = especialidad
    self.docente = docente
    self.unidades = unidades
    self.bibliografia = bibliografia

  def ficha(self):
    print('Nombre:', self.nombre, '\nAno:', self.ano, '\nEspecialidad:', self.especialidad, '\nDocente:', self.docente, '\nUnidades:', self.unidades, '\nBibliografia', self.bibliografia)

asignatura('Matematica', 5, 'Tecnica', 'Mateucci', ['Vectores', 'Imaginarios', 'Limites', 'Integrales'], ['Matematics by Semanski', 'Matematica for dummies v54', 'Apunte de catedra']).ficha()
