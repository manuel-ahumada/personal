import math

class numero_complejo:
  def __init__(self, option, a, b):
    self.option = option
    if option:
      self.x = a
      self.y = b
      self.m = math.sqrt(self.x**2+self.y**2)
      self.arg = math.atan(self.y/self.x)
    else:
      self.m = a
      self.arg = b
      self.x = self.m*math.cos(self.arg)
      self.y = self.m*math.sin(self.arg)

  def print_binomial(self):
    print('Your number is', self.x, '+', self.y, 'i')

  def print_polar(self):
    print('Your number is |', self.m, '| ( Cos(', self.arg, ') + iSin(', self.arg, ') )')

  def sum(numA, numB):
    return numero_complejo(1, numA.x + numB.x, numA.y + numB.y)

  def substraction(numA, numB):
    return numero_complejo(1, numA.x - numB.y, numA.y - numB.y)

  def product(numA, numB):
    return numero_complejo(1, (numA.x*numB.x-numA.y*numB.y), (numA.x*numB.y-numA.y*numB.x))

  def division(numA, numB):
    return numero_complejo(1, ((numA.x*numB.x)+(numA.y*numB.y))/(numB.x**2+numB.y**2), ((numA.y*numB.x)-(numA.x*numB.y))/(numB.x**2+numB.y**2))

number1 = numero_complejo(1, 2, 4)
number2 = numero_complejo(0, 4, 2)

numero_complejo.product(number1, number2).print_binomial()
