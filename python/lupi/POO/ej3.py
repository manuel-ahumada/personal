class alumno:
  def __init__(self, nombre, apellido, dni, fechaNacimiento, sexo, direccion, peso, altura):
    self.nombre = nombre
    self.apellido = apellido
    self.dni = dni
    self.fechaNacimiento = fechaNacimiento
    self.sexo = sexo
    self.direccion = direccion
    self.peso = peso
    self.altura = altura

  def edad(self):
    return 2021-int(self.fechaNacimiento[len(self.fechaNacimiento)-4:])

  def imc(self):
    return self.peso / (self.altura**2)

  def ficha(self):
    print('Nombre:', self.nombre, '\nApellido:', self.apellido, '\nDni:', self.dni, '\nFecha de nacimiento:', self.fechaNacimiento, '\nSexo:', self.sexo, '\nDireccion:', self.direccion, '\nPeso:', self.peso, '\nAltura:', self.altura, '\nEdad:', self.edad(), '\nIMC:', self.imc())

alumno('Manuel', 'Ahumada', 44729959, '09-04-2003', 'M', 'Direccion falsa 1042', 62, 1.79).ficha()
