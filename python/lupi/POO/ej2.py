class automovil:
  def __init__(self, marca, modelo, fabricacion, patente, kilometraje):
    self.marca = marca
    self.modelo = modelo
    self.fabricacion = fabricacion
    self.patente = patente
    self.kilometraje = kilometraje

  def edad(self):
    return int(2021 - self.fabricacion)

  def promKilometraje(self):
    return self.kilometraje / automovil.edad(self)
  
  def ficha(self):
    print('Marca:', self.marca, '\nModelo:', self.modelo, '\nFecha de Fabricacion:', self.fabricacion, '\nPatente:', self.patente, '\nKilometraje:', self.kilometraje, '\nEdad:', self.edad(), '\nKilometraje promedio Anual:', self.promKilometraje());

automovil('Ford', 'Fiesta', 2014, 'ESF361', 146980).ficha()
